//
//  MarkderDetectorFacade.h
//  ARMarkerDemo
//
//  Created by Kausch Thomas, ENT-BIZ-PSR-AUG-AUS on 09/03/16.
//  Copyright © 2016 Kausch Thomas. All rights reserved.
//

#ifndef MarkderDetectorFacade_h
#define MarkderDetectorFacade_h


#include <vector>
#include <memory>

#include "BGRAVideoFrame.h"
#include "GeometryTypes.hpp"
#include "CameraCalibration.hpp"

class MarkerDetectionFacade {
    
public:
    
    virtual void processFrame(const BGRAVideoFrame& frame) = 0;
    virtual const std::vector<Transformation>& getTransformations() const = 0;
    virtual ~MarkerDetectionFacade() {}
    
};

std::auto_ptr<MarkerDetectionFacade> createMarkerDetection(CameraCalibration calibration);





#endif /* MarkderDetectorFacade_h */
