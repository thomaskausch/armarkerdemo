//
//  ViewController.m
//  ARMarkerDemo
//
//  Created by Kausch Thomas, ENT-BIZ-PSR-AUG-AUS on 09/03/16.
//  Copyright © 2016 Kausch Thomas. All rights reserved.
//

#import "ViewController.h"



@implementation ViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    m_videoSource = [[VideoSource alloc]init];
    m_videoSource.delegate = self;
    
    m_pipeline = createMarkerDetection([m_videoSource getCalibration]);
    [m_videoSource startWithDevicePosition:AVCaptureDevicePositionBack];
}

- (void)viewDidUnload
{
    self.glview = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.glview initContext];
    

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(avCaptureInputPortFormatDescriptionDidChangeNotification:)
                                                 name:AVCaptureInputPortFormatDescriptionDidChangeNotification object:nil];
    
    
    [super viewWillAppear:animated];
}



- (void)avCaptureInputPortFormatDescriptionDidChangeNotification:(NSNotification *)notification {

    CGSize frameSize = [m_videoSource getFrameSize];
    m_renderer = createVisualizationController(self.glview, [m_videoSource getCalibration], frameSize);
    
    
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return interfaceOrientation == UIInterfaceOrientationLandscapeRight;
    //return interfaceOrientation == UIInterfaceOrientationPortrait;
}

#pragma mark - VideoSourceDelegate
-(void)frameReady:(BGRAVideoFrame) frame
{
    // Start upload new frame to video memory in main thread
    dispatch_async( dispatch_get_main_queue(), ^{
        [m_renderer updateBackground:frame];
    });
    

    // And perform processing in current thread
    m_pipeline->processFrame(frame);
    
    // When it's done we query rendering from main thread
    dispatch_async( dispatch_get_main_queue(), ^{

        [m_renderer setTransformationList:(m_pipeline->getTransformations)()];
        [m_renderer drawFrame];
    });
}

@end
