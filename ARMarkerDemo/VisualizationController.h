//
//  VisualizationController.h
//  ARMarkderDemo
//
//  Created by Kausch Thomas, ENT-BIZ-PSR-AUG-AUS on 09/03/16.
//  Copyright © 2016 Kausch Thomas. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <vector>


#import "VideoSource.h"
#import "GeometryTypes.hpp"
#import "EAGLView.h"

@protocol VisualizationController <NSObject>
-(void) drawFrame;
-(void) updateBackground:(BGRAVideoFrame) frame;
-(void) setTransformationList:(const std::vector<Transformation>&) transformations;
@end

id<VisualizationController> createVisualizationController(EAGLView * view, CameraCalibration calibration, CGSize size);