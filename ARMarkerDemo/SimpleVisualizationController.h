//
//  ARMarkerDemo
//  SimpleVisualizationController.h
//
//  Created by Kausch Thomas, ENT-BIZ-PSR-AUG-AUS on 09/03/16.
//  Copyright © 2016 Kausch Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VisualizationController.h"

#import "EAGLView.h"

@interface SimpleVisualizationController : NSObject<VisualizationController>
{
  EAGLView * m_glview;
  GLuint m_backgroundTextureId;
  std::vector<Transformation> m_transformations;
  CameraCalibration m_calibration;
  CGSize m_frameSize;
}

-(id) initWithGLView:(EAGLView*)view calibration:(CameraCalibration) calibration frameSize:(CGSize) size;

-(void) drawFrame;
-(void) updateBackground:(BGRAVideoFrame) frame;
-(void) setTransformationList:(const std::vector<Transformation>&) transformations;

@end
