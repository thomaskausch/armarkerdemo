//
//  ViewController.h
//  ARMarkerDemo
//
//  Created by Kausch Thomas, ENT-BIZ-PSR-AUG-AUS on 09/03/16.
//  Copyright © 2016 Kausch Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EAGLView.h"
#import "VideoSource.h"
#import "MarkerDetectionFacade.hpp"
#import "VisualizationController.h"

@interface ViewController : UIViewController<VideoSourceDelegate>
{
    VideoSource *                        m_videoSource;
    std::auto_ptr<MarkerDetectionFacade> m_pipeline;
    id<VisualizationController> m_renderer;
}
@property (strong, nonatomic) IBOutlet EAGLView *glview;
@end

