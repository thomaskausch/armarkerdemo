//
//  MarkerDetector.hpp
//  ARMarkerDemo
//
//  Created by Kausch Thomas, ENT-BIZ-PSR-AUG-AUS on 09/03/16.
//  Copyright © 2016 Kausch Thomas. All rights reserved.
//

#ifndef MarkerDetector_hpp
#define MarkerDetector_hpp

#include <vector>
#include <opencv2/opencv.hpp>

#include "MarkerDetectionFacade.hpp"
#include "CameraCalibration.hpp"
#include "BGRAVideoFrame.h"
#include "Marker.hpp"


class MarkerDetector : public MarkerDetectionFacade {
    
public:
    
    /**
     * Initialize a new instance of marker detector object
     * @calibration[in] - Camera calibration (intrinsic and distorsion components) necessary for pose estimation.
     */
    MarkerDetector(CameraCalibration calibration);
    
    void processFrame(const BGRAVideoFrame& frame);
    
    
    const std::vector<Transformation>& getTransformations() const;
    
protected:
    bool findMarkers(const BGRAVideoFrame& frame, std::vector<Marker>& detectedMarkers);
    
    void prepareImage(const cv::Mat& bgraMat, cv::Mat& grayscale);
    void performThreshold(const cv::Mat& grayscale, cv::Mat& thresholdImg);
    void findContours(const cv::Mat& thresholdImg, std::vector<std::vector<cv::Point> >& contours,int minContourPointsAllowed);
    void findMarkerCandidates(const std::vector<std::vector<cv::Point> >& contours, std::vector<Marker>& detectedMarkers);
    void detectMarkers(const cv::Mat& grayscale, std::vector<Marker>& detectedMarkers);
    void estimatePosition(std::vector<Marker>& detectedMarkers);
    float perimeter(const std::vector<cv::Point2f> &a);
    bool isInto(cv::Mat &contour, std::vector<cv::Point2f> &b);
    
    
private:
    float m_minContourLengthAllowed;
    
    cv::Size markerSize;
    cv::Mat camMatrix;
    cv::Mat distCoeff;
    std::vector<Transformation> m_transformations;
    
    cv::Mat m_grayscaleImage;
    cv::Mat m_thresholdImg;
    std::vector<std::vector<cv::Point> > m_contours;
    std::vector<cv::Point3f> m_markerCorners3d;
    std::vector<cv::Point2f> m_markerCorners2d;

};



#endif /* MarkerDetector_hpp */
