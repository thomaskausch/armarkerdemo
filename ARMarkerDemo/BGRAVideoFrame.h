//
//  BGRAVideoFrame.h
//  ARMarkerDemo
//
//  Created by Kausch Thomas, ENT-BIZ-PSR-AUG-AUS on 09/03/16.
//  Copyright © 2016 Kausch Thomas. All rights reserved.
//

#ifndef BGRAVideoFrame_h
#define BGRAVideoFrame_h


// A helper struct presenting interleaved BGRA image in memory.
struct BGRAVideoFrame
{
    size_t width;
    size_t height;
    size_t stride;
    
    uint8_t * data;
};


#endif /* BGRAVideoFrame_h */
