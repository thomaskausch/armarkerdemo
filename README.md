# Marker based Augmented Reality #
*Augmented reality* (AR) is a live view of a real-world environment whose elements are augmented by computer-generated graphics. With the help of advanced AR technology for example, adding computer vision and object recognition the information about the surrounding world of the user becomes interactive and can be digitally manipulated.

This project contains an AR App for iPhone device that uses binary markers to draw some artificial objects on the images acquired from the camera. It covers the following topics:

* Camera handling
* Marker detection
* Marker identification
* Marker code recognition
* Camera position estimation
* Adding virtual 3D object on top of the marker
* Rendering the virtual 3D object

Checkout the project open and open in XCode. The project was written with Xcode 7.2 and tested on iPhone 6.

## Adding OpenCV framework ##
Starting from version 2.4.2, OpenCV library is of cially supported on the iOS platform and you can download the distribution package from the library website at http:// opencv.org/. The OpenCV for iOS link points to the compressed OpenCV framework.

Add the framework to your project and add OpenCV headers to the project's precompiled headers. The precompiled headers are a great feature to speed up compilation time. Have a look at your .pch file and add the following section. 


```
#!objectivec

   #ifdef __cplusplus
   #include <opencv2/opencv.hpp>
   #endif

```



## App from 20'000 feet ##

The application that we are going to write will have only one view. This view will be used to present the rendered picture. Our ViewController class will contain three major components that each AR application should have:

* Video source (VideoSource.h)
* Processing pipeline (MarkerDetector.hpp)
* Visualisation engine (VisualisationController.h)

The video source is responsible for providing new frames taken from the built-in camera to the user code. This means that the video source should be capable of choosing a camera device (front- or back-facing camera), adjusting its parameters (such as resolution of the captured video, white balance, and shutter speed), and grabbing frames without freezing the main UI.

The image processing routine will be encapsulated in the MarkerDetector class. This class provides a very thin interface to user code. Usually it's a set of functions like processFrame and getResult. Actually that's all that ViewController should know about. We must not expose low-level data structures and algorithms to the view layer without strong necessity. 

VisualizationController contains all logic concerned with visualization of the Augmented Reality on our view. VisualizationController is also a facade that hides a particular implementation of the rendering engine. Low code coherence gives us freedom to change these components without the need to rewrite the rest of your code.

The main processing routine starts from receiving a new frame from the video source. This triggers video source to inform the user code about this event with a callback. ViewController handles this callback and performs the following operations:

1. Sends a new frame to the visualisation controller.
2. Performs processing of the new frame using our pipline.
3. Sends the detected markers to the visualization stage.
4. Renders a scene.

The rendering of an AR scene includes the drawing of a background image that has a content of the last received frame; artificial 3D objects are drawn later on. When we send a new frame for visualization, we are copying image data to internal buffers of the rendering engine. This is not actual rendering yet; we are just updating the text with a new bitmap.

The second step is the processing of new frame and marker detection. We pass our image as input and as a result receive a list of the markers detected on it. These markers are passed to the visualization controller, which knows how to deal with them.

![sequenceDiagram.png](https://bitbucket.org/repo/BEbgG4/images/3867780511-sequenceDiagram.png)


## Marker Detection ##

A marker is usually designed as a rectangle image holding black and white areas inside it. Due to known limitations, the marker detection procedure is a simple one. First of all we need to find closed contours on the input image and unwarp the image inside it to a rectangle and then check this against our marker model. In our app the 5 x 5 marker will be used. Here is what it looks like:

![Screen Shot 2016-03-10 at 14.06.31.png](https://bitbucket.org/repo/BEbgG4/images/1103464985-Screen%20Shot%202016-03-10%20at%2014.06.31.png)

To help understand the marker detection routine, a step-by-step processing on the following video frame will be shown.

![Original video frame](https://bitbucket.org/repo/BEbgG4/images/3469236025-original.png)


## Marker identification ##

Here is the workflow of the marker detection routine:

* Convert the input image to grayscale. 
* Perform binary threshold operation. 
* Detect contours.
* Search for possible markers.
* Detect and decode markers. Estimate marker 3D pose. 

### Grayscale conversion ###

The conversion to grayscale is necessary because markers usually contain only black and white blocks and it's much easier to operate with them on grayscale images. Fortunately, OpenCV color conversion is simple enough. 

```
#!cpp

  void MarkerDetector::prepareImage(const cv::Mat& bgraMat, cv::Mat&
   grayscale)
   {
     // Convert to grayscale
     cv::cvtColor(bgraMat, grayscale, CV_BGRA2GRAY);
   }

```

This function will convert the input BGRA image to grayscale and place the result image in the second parameter. 

### Image binarization ###

The binarization operation will transform each pixel of an image to black (zero intensity) or white (full intensity). This step is required to  find contours. There
are several threshold methods; each has strong and weak sides. The easiest and fastest method is absolute threshold. In this method the resulting value depends on current pixel intensity and some threshold value. If pixel intensity is greater than the threshold value, the result will be white (255); otherwise it will be black (0).

This method has a huge disadvantage—it depends on lighting conditions and
soft intensity changes. The more preferable method is the adaptive threshold. The major difference of this method is the use of all pixels in given radius around the examined pixel. Using average intensity gives good results and secures more robust corner detection.

The following code snippet shows the MarkerDetector function:


```
#!cpp

   void MarkerDetector::performThreshold(const cv::Mat& grayscale,
   cv::Mat& thresholdImg)
   {
     cv::adaptiveThreshold(grayscale,   // Input image
                           thresholdImg,// Result binary image
                           255,         //
                           cv::ADAPTIVE_THRESH_GAUSSIAN_C, //
                           cv::THRESH_BINARY_INV, //
                           7, //
                           7  //
                           );
}

```

After applying adaptive threshold to the input image, the resulting image looks similar to the following: 

![Image after binarization](https://bitbucket.org/repo/BEbgG4/images/503708319-Screen%20Shot%202016-03-16%20at%2012.28.42.png)


### Contours detection ###

The cv::findCountours function will detect contours on the input binary image: 


```
#!cpp

void MarkerDetector::findContours(const cv::Mat& thresholdImg,
                                     std::vector<std::vector<cv::Point>
   >& contours,
   int minContourPointsAllowed)
   {
       std::vector< std::vector<cv::Point> > allContours;
       cv::findContours(thresholdImg, allContours, CV_RETR_LIST, CV_
   CHAIN_APPROX_NONE);
       contours.clear();
       for (size_t i=0; i<allContours.size(); i++)
       {
           int contourSize = allContours[i].size();
           if (contourSize > minContourPointsAllowed)
           {
               contours.push_back(allContours[i]);
           }
} }


```

The return value of this function is a list of polygons where each polygon represents a single contour. The function skips contours that have their perimeter in pixels value set to be less than the value of the minContourPointsAllowed variable. This is because we are not interested in small contours. The following  gure shows the visualization of detected contours:

![After Contour detection](https://bitbucket.org/repo/BEbgG4/images/3800223653-Screen%20Shot%202016-03-16%20at%2012.32.35.png)

### Candidates search ###

After  finding contours, the polygon approximation stage is performed. This is done to decrease the number of points that describe the contour shape. It's a good quality check to  filter out areas without markers because they can always be represented with a polygon that contains four vertices. If the approximated polygon has more than or fewer than 4 vertices, it's definitely not what we are looking for. For the concert implementation have a look at the  *MarkerDetector::findCandidates* method. It does more or less the following: 

* Approximate each contours by a polygon
* The approximated polygon has to fulfil the following criteria: Must have only 4 edges, must be convex, distance between consecutive points is large enough.

Now we have obtained a list of parallelepipeds that are likely to be the markers. To verify whether they are markers or not, we need to perform three steps:

1. First, we should remove the perspective projection so as to obtain a frontal view of the rectangle area.
2. Then we perform thresholding of the image using the Otsu algorithm. This algorithm assumes a bimodal distribution and  finds the threshold value that maximizes the extra-class variance while keeping a low intra-class variance.
3. Finally we perform identification of the marker code. If it is a marker, it has an internal code. The marker is divided into a 7 x 7 grid, of which the internal 5 x 5 cells contain ID information. The rest correspond to the external black border. Here, we  first check whether the external black border is present. Then we read the internal 5 x 5 cells and check if they provide a valid code. It might be required to rotate the code to get the valid one.

To get the rectangle marker image, we have to unwarp the input image using perspective transformation. This matrix can be calculated with the help of the *cv::getPerspectiveTransform* function. It  finds the perspective transformation from four pairs of corresponding points. The  first argument is the marker coordinates in image space and the second point corresponds to the coordinates of the square marker image. Estimated transformation will transform the marker to square form and let us analyze it:


```
#!cpp

   cv::Mat canonicalMarker;
   Marker& marker = detectedMarkers[i];
   // Find the perspective transfomation that brings current marker to
   rectangular form
   cv::Mat M = cv::getPerspectiveTransform(marker.points, m_
   markerCorners2d);
   // Transform image to get a canonical marker image
   cv::warpPerspective(grayscale, canonicalMarker,  M, markerSize);

```

Image warping transforms our image to a rectangle form using perspective transformation:

![Image after warping](https://bitbucket.org/repo/BEbgG4/images/81603098-Screen%20Shot%202016-03-16%20at%2012.56.20.png)

Now we can test the image to verify if it is a valid marker image. Then we try to extract the bit mask with the marker code. As we expect our marker to contain only black and white colors, we can perform Otsu thresholding to remove gray pixels and leave only black and white pixels:


```
#!cpp

   //threshold image
   cv::threshold(markerImage, markerImage, 125, 255, cv::THRESH_BINARY |
   cv::THRESH_OTSU);
```

![Marker image after Otsu thresholding](https://bitbucket.org/repo/BEbgG4/images/3752092486-Screen%20Shot%202016-03-16%20at%2012.59.25.png)


### Marker code recognition ### 

Each marker has an internal code given by 5 words of 5 bits each. The codification employed is a slight modification of the hamming code. In total, each word has only 2 bits of information out of the 5 bits employed. The other 3 are employed for error detection. As a consequence, we can have up to 1024 different IDs.

The main difference with the hamming code is that the first bit (parity of bits
3 and 5) is inverted. So, ID 0 (which in hamming code is 00000) becomes 10000
in our code. The idea is to prevent a completely black rectangle from being a valid marker ID, with the goal of reducing the likelihood of false positives with objects of the environment.

Counting the number of black and white pixels for each cell gives us a 5 x 5-bit mask with marker code. To count the number of non-zero pixels on a certain image, the cv::countNonZero function is used. This function counts non-zero array elements from a given 1D or 2D array. The cv::Mat type can return a subimage view—a new instance of cv::Mat that contains a portion of the original image. For example, if you have a cv::Mat of size 400 x 400, the following piece of code will create a submatrix for the 50 x 50 image block starting from (10, 10):


```
#!cpp

   cv::Mat src(400,400,CV_8UC1);
   cv::Rect r(10,10,50,50);
   cv::Mat subView = src(r);

```

### Reading marker code ###

Using this technique, we can easily find black and white cells on the marker board. Take a look at the following figure. The same marker can have four possible representations depending on the camera's point of view:

![Rotated markders](https://bitbucket.org/repo/BEbgG4/images/3817693749-Screen%20Shot%202016-03-16%20at%2013.43.36.png)

As there are four possible orientations of the marker picture, we have to find the correct marker position. Remember, we introduced three parity bits for each two bits of information. With their help we can find the hamming distance for each possible marker orientation. The correct marker position will have zero hamming distance error, while the other rotations won't.

Here is a code snippet that rotates the bit matrix four times and  finds the correct marker orientation:


```
#!cpp

 //check all possible rotations
    cv::Mat rotations[4];
    int distances[4];
    
    rotations[0] = bitMatrix;
    distances[0] = hammDistMarker(rotations[0]);
    
    std::pair<int,int> minDist(distances[0],0);
    
    for (int i=1; i<4; i++)
    {
        //get the hamming distance to the nearest possible word
        rotations[i] = rotate(rotations[i-1]);
        distances[i] = hammDistMarker(rotations[i]);
        
        if (distances[i] < minDist.first)
        {
            minDist.first  = distances[i];
            minDist.second = i;
        }
    }
    
    nRotations = minDist.second;
    if (minDist.first == 0)
    {
        return mat2id(rotations[minDist.second]);
    }


```

This code  finds the orientation of the bit matrix in such a way that it gives minimal error for the hamming distance metric. This error should be zero for correct marker ID; if it's not, it means that we encountered a wrong marker pattern (corrupted image or false-positive marker detection).


### Marker location refinement ###
After  finding the right marker orientation, we rotate the marker's corners respectively to conform to their order - *MarkerDetector::detectMarkers* .

After detecting a marker and decoding its ID, we will refine its corners. This operation will help us in the next step when we will estimate the marker position in 3D. To find the corner location with subpixel accuracy, the cv::cornerSubPix function is used.


## Placing a marker in 3D ##
Augmented Reality tries to fuse the real-world object with virtual content. To place a 3D model in a scene, we need to know its pose with regard to a camera that we use to obtain the video frames. We will use a Euclidian transformation in the Cartesian coordinate system to represent such a pose.

The position of the marker in 3D and its corresponding projection in 2D is restricted by the following equation:


```
#!python

P = A * [R|T] * M;
```
 

Where:
• M denotes a point in a 3D space
• [R|T] denotes a [3|4] matrix representing a Euclidian transformation
• A denotes a camera matrix or a matrix of intrinsic parameters
• P denotes projection of M in screen space

After performing the marker detection step we now know the position of the four marker  corners in 2D (projections in screen space). In the next section we will learn how to obtain the A matrix and M vector parameters and calculate the [R|T] transformation.

### Camera calibration ###

Each camera lens has unique parameters, such as focal length, principal point, and lens distortion model. The process of  finding intrinsic camera parameters is called camera calibration. The camera calibration process is important for Augmented Reality applications because it describes the perspective transformation and lens distortion on an output image. To achieve the best user experience with Augmented Reality, visualization of an augmented object should be done using the same perspective projection.

For this sample we have constant internal parameters for iOS devices.


### Marker pose estimation ###
TBD